package org.md.json2xml.JSONobjects;

public class RootJSONObject {

	private int field1;
	private String field2;
	private String array[];
	private ChildJSONObject childObject;
	
	public int getField1() {
		return field1;
	}
	public void setField1(int field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String[] getArray() {
		return array;
	}
	public void setArray(String[] array) {
		this.array = array;
	}
	public ChildJSONObject getChildObject() {
		return childObject;
	}
	public void setChildObject(ChildJSONObject childObject) {
		this.childObject = childObject;
	}	
}
