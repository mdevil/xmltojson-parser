package org.md.json2xml.XMLobjects;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;


@JacksonXmlRootElement(localName = "rootXMLObject")
//Whether to use namespaces, add the namespace attribute in all entries (root element and xmlProperty):
//@JacksonXmlRootElement(localName = "rootXMLobj", namespace = "http://myNamespace.com")
public class RootXMLObject {
	
	@JacksonXmlProperty(localName = "field1")
	private int field1;
	
	@JacksonXmlProperty(localName = "field2")
	private String field2;
	
	@JacksonXmlProperty(localName = "list")
	@JacksonXmlElementWrapper(useWrapping = false)
	private String[] list;
	
	@JacksonXmlProperty(localName = "childObject")
	private ChildXMLObject childObject;

	public int getField1() {
		return field1;
	}

	public void setField1(int field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String[] getList() {
		return list;
	}

	public void setList(String[] list) {
		this.list = list;
	}

	public ChildXMLObject getChildObject() {
		return childObject;
	}

	public void setChildObject(ChildXMLObject childObject) {
		this.childObject = childObject;
	}	
}
