package org.md.json2xml.XMLobjects;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;



public class ChildXMLObject {
	
	@JacksonXmlProperty(localName = "childField1")
	private String childField1;
	@JacksonXmlProperty(localName = "childField2")
	private String childField2;
	
	public String getChildField1() {
		return childField1;
	}
	public void setChildField1(String childField1) {
		this.childField1 = childField1;
	}
	public String getChildField2() {
		return childField2;
	}
	public void setChildField2(String childField2) {
		this.childField2 = childField2;
	}
}
