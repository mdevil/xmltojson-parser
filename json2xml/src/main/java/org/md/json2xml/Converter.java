package org.md.json2xml;

import java.io.IOException;

import org.md.json2xml.JSONobjects.RootJSONObject;
import org.md.json2xml.JSONobjects.ChildJSONObject;
import org.md.json2xml.XMLobjects.RootXMLObject;
import org.md.json2xml.XMLobjects.ChildXMLObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class Converter {

	public static String jsonToXml(String jsonString) throws Exception
	{
		try{
			// Transform json to java
			RootJSONObject jsonObject = Converter.jsonToJava(jsonString);
				
			// Create xml container objects
			RootXMLObject xmlObject = new RootXMLObject();
			ChildXMLObject childXmlObject = new ChildXMLObject();
			
			// Mapping between java objects
			xmlObject.setField1(jsonObject.getField1());
			xmlObject.setField2(jsonObject.getField2());
			xmlObject.setList(jsonObject.getArray());
			childXmlObject.setChildField1(jsonObject.getChildObject().getChildField1());
			childXmlObject.setChildField2(jsonObject.getChildObject().getChildField2());
			xmlObject.setChildObject(childXmlObject);
					
			// Transform java to xml
			return (Converter.javaToXML(xmlObject));
		}
		catch(Exception e)
		{ 
			throw e; 
		}
	}
	
	
	public static String xmlToJson(String xmlString) throws Exception
	{
		try
		{
		// Transform xml to java
		RootXMLObject xmlObject = Converter.xmlToJava(xmlString);
		
		// Create JSON container objects
		RootJSONObject jsonObject = new RootJSONObject();
		ChildJSONObject childJsonObject = new ChildJSONObject();
		
		// Mapping between java objects
		jsonObject.setField1(xmlObject.getField1());
		jsonObject.setField2(xmlObject.getField2());
		jsonObject.setArray(xmlObject.getList());
		childJsonObject.setChildField1(xmlObject.getChildObject().getChildField1());
		childJsonObject.setChildField2(xmlObject.getChildObject().getChildField2());
		jsonObject.setChildObject(childJsonObject);
		
		// Transform java to json
		return(Converter.javaToJson(jsonObject));
		}
		catch (Exception e)
		{
			throw e;
		}
	}
		
	private static RootJSONObject jsonToJava (String jsonString) throws JsonParseException, JsonMappingException, IOException
	{
		ObjectMapper jsonMapper = new ObjectMapper();
		RootJSONObject jsonObject = jsonMapper.readValue(jsonString, RootJSONObject.class);
		return jsonObject;
	}
	
	private static String javaToJson (RootJSONObject objecteJson) throws JsonProcessingException
	{
		ObjectMapper jsonMapper = new ObjectMapper();
		String jsonString = jsonMapper.writeValueAsString(objecteJson);
		return (jsonString);
	}
		
	private static RootXMLObject xmlToJava (String xmlString) throws JsonParseException, JsonMappingException, IOException 
	{
		XmlMapper xmlMapper = new XmlMapper();
		RootXMLObject xmlObject = xmlMapper.readValue(xmlString,RootXMLObject.class);
		return xmlObject;
	}
	
	
	private static String javaToXML (RootXMLObject objecteXml) throws JsonProcessingException 
	{
		XmlMapper xmlMapper = new XmlMapper();
		String xmlString = xmlMapper.writeValueAsString(objecteXml);
		return xmlString;
	}	
}
