package json2xml;

import org.junit.Test;
import org.md.json2xml.Converter;

public class ConverterTest {

	
	@Test
	public void jsonToXml() throws Exception
	{
		String jsonString = "{\"field1\": 1, \"field2\": \"valorField2\", \"array\": [ \"Array1Value\", \"Array2Value\" ], \"childObject\": { \"childField1\": \"childField1Value\", \"childField2\": \"childField2Value\"}}";
		System.out.println("Original JSON: " + jsonString);
		String xmlString = Converter.jsonToXml(jsonString);
		System.out.println("Result XML: " + xmlString);
		System.out.println("Converted JSON: " + Converter.xmlToJson(xmlString));
	}
	
	@Test
	public void xmlToJson() throws Exception
	{
		String xmlString = "<rootXMLObject><field1>12</field1><field2>Field2Value</field2><list>val1</list><list>val2</list><childObject><childField1>Son1Value</childField1><childField2>Son2Value</childField2></childObject></rootXMLObject>";
		System.out.println("Original XML: " + xmlString);
		String jsonString = Converter.xmlToJson(xmlString);
		System.out.println("Result JSON: " + jsonString);
		System.out.println("Converted XML: " + Converter.jsonToXml(jsonString));
		
	}
	/*
	@Test
	public void jsonAJava() throws IOException
	{
		String jsonString = "{ \"field1\": 1, \"field2\": \"valorField2\", \"array\": [ \"Array1Value\", \"Array2Value\" ], \"childObject\": { \"childField1\": \"childField1Value\", \"childField2\": \"childField2Value\" } }";
		ObjecteJsonArrel objecteJson = Conversor.jsonAJava(jsonString);
		System.out.println(objecteJson.getField2());
	}
	
	@Test
	public void xmlAJava() throws IOException
	{
		String xmlString = "<rootXMLObject><field1>12</field1><field2>Field2Value</field2><list>val1</list>	<list>val2</list><childObject><childField1>Son1Value</childField1><childField2>Son2Value</childField2></childObject></rootXMLObject>";
		ObjecteXmlArrel objecteXml = Conversor.xmlAJava(xmlString);
		System.out.println(objecteXml.getCampo2());
	}
	
	@Test
	public void javaAJson() throws IOException
	{
		ObjecteJsonFill objJsonFill= new ObjecteJsonFill();
		objJsonFill.setField1Fill("field1");
		objJsonFill.setField2Fill("field2");
		ObjecteJsonArrel objJsonArrel = new ObjecteJsonArrel();
		objJsonArrel.setField1(55);
		objJsonArrel.setField2("Valor2");
		objJsonArrel.setArray(new String[] {"valorArray1","valorArray2"});
		objJsonArrel.setObjecteFill(objJsonFill);
		
		String jsonString = Conversor.javaAJson(objJsonArrel);
		System.out.println(jsonString);
	}
	
	@Test
	public void javaAXml() throws JsonProcessingException 
	{
		ObjecteXmlFill objXmlFill = new ObjecteXmlFill();
		objXmlFill.setCampo1Hijo("hijo1");
		objXmlFill.setCampo2Hijo("hijo2");
		ObjecteXmlArrel rootXMLObject = new ObjecteXmlArrel();
		rootXMLObject.setCampo1(444);
		rootXMLObject.setCampo2("valCampo2");
		rootXMLObject.setLista(new String[] {"val1","val2"});
		rootXMLObject.setObjecteFill(objXmlFill);
		
		String xmlString = Conversor.javaAXML(rootXMLObject);
		System.out.println(xmlString);
		
	}*/
	
}
